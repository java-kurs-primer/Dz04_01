package rs.ac.singidunum.dz04_01;

import java.util.HashSet;
import java.util.Set;

public class Kontakt {

    private String ime;
    private String prezime;
    private Set<String> tels;
    private Set<String> emails;

    private Kontakt() {
        this.tels = new HashSet<>();
        this.emails = new HashSet<>();
    }

    public Kontakt(String ime, String prezime) {
        this();
        this.ime = ime;
        this.prezime = prezime;
    }

    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public Set<String> getTel() {
        return tels;
    }

    public void setTel(Set<String> tel) {
        this.tels = tel;
    }

    public void addTel(String tel) {
        this.tels.add(tel);
    }

    public Set<String> getEmail() {
        return emails;
    }

    public void setEmail(Set<String> email) {
        this.emails = email;
    }

    public void addEmail(String email) {
        this.emails.add(email);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[<");
        sb.append(ime);
        sb.append(" ");
        sb.append(prezime);
        sb.append(">: ");
        if (!this.tels.isEmpty()) {
            sb.append(String.join(", ", this.tels));
        }
        if (!this.emails.isEmpty()) {
            sb.append(" :: ");
            sb.append(String.join(", ", this.emails));
        }
        sb.append("]");

        return sb.toString();
    }

}
