package rs.ac.singidunum.dz04_01;

import java.io.IOException;

public class ImenikDemoNew {

    public static void main(String[] args) throws IOException {

        IImenik mojImenik = new ImenikImpl();
        mojImenik.ucitajXmlImenik("resources/imenik.xml");

        System.out.println("Sadrzaj mog imenika");
        mojImenik.prikaziImenika();

        System.out.println("Dodavanje kontakta");
        Kontakt noviKontakt = new Kontakt("Ime", "Prezime");
        noviKontakt.addTel("011/1234567");
        noviKontakt.addTel("011/2020202");
        noviKontakt.addEmail("a@gmail.com");
        mojImenik.dodajKontakt(noviKontakt);

        System.out.println("Novi sadrzaj imenika");
        System.out.println(mojImenik.prikaziImenika());
    }
}
