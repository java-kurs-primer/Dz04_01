package rs.ac.singidunum.dz04_01;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImenikImpl implements IImenik {

    private final List<Kontakt> listaKontakata;

    public ImenikImpl() {
        this.listaKontakata = new ArrayList<>();
    }

    @Override
    public void ucitajXmlImenik(String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void sacuvajXmlImenik(String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void dodajKontakt(Kontakt kontakt) {
        this.listaKontakata.add(kontakt);
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Kontakt> nadjiKontakt(String ime, String prezime) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Kontakt> nadjiKontaktTel(String tel) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Kontakt> nadjiKontaktImejl(String imejl) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void obrisiKontakt(String ime, String prezime) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Kontakt k : this.listaKontakata) {
            sb.append(k.toString());
            sb.append("\n");
        }
        sb.append("\n");

        return sb.toString();
    }

}
