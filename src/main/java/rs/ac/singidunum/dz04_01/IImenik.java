package rs.ac.singidunum.dz04_01;

import java.io.IOException;
import java.util.List;

public interface IImenik {

    public void ucitajXmlImenik(String filePath) throws IOException;

    public void sacuvajXmlImenik(String filePath) throws IOException;

    default public String prikaziImenika() {
        return this.toString();
    }

    public void dodajKontakt(Kontakt kontakt);

    public List<Kontakt> nadjiKontakt(String ime, String prezime);

    public List<Kontakt> nadjiKontaktTel(String telefon);

    public List<Kontakt> nadjiKontaktImejl(String imejl);

    public void obrisiKontakt(String ime, String prezime);

}
