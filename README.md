# Dz04_01

#### Interfejs za implementaciju
Dat je insterfejs `IImenik` za rad sa telefonskim imenikom koji se čuva u .xml formatu:
```java
public interface IImenik {

	public void ucitajImenik(String filePath) throws IOException;

  public void sacuvajXmlImenik(String filePath) throws IOException;

	default public String prikaziImenika() {
		return this.toString();
	}

	public void dodajKontakt(Kontakt kontakt);

	public Kontakt nadjiKontakt(String ime, String prezime);

	public Kontakt nadjiKontakt(String telefon);

	public void obrisiKontakt(String ime, String prezime);

}
```

#### Pomoćna klasa - `Kontakt`
Data je klasa `Kontakt` (deo):
```java
public class Kontakt {
    private String ime;
    private String prezime;
    private Set<String> tels;
    private Set<String> emails;

    private Kontakt() {
        this.tels = new HashSet<>();
        this.emails = new HashSet<>();
    }

    public Kontakt(String ime, String prezime) {
        this();
        this.ime = ime;
        this.prezime = prezime;
    }
}
```
Klasa `Kontakt` služi za rad sa pojedinačnim kontaktom u adresaru.  
Kontakt sadrži ime, prezime, nula ili više brojeva telefona i imejl adresa.  
U klasi `Kontakt` konstruktor `Kontakt()` je privatni, a jedino je javni konstruktor `Kontakt(String ime, String prezime)`.  
To znači da se pri pravljenju nove instance kontakta u konstruktoru mora proslediti ime i prezime.  
U klasi `Kontakt` još je implementirana metoda `String toString()` koja služi za ispis kontakta.  

**Napomena:**
Ne menjati sadržaj ove klase.  


#### Format .xml fajla
Format .xml fajla u kome se čuva imenik je sledeći:
```xml
<imenik>
    <kontakt>
        <ime>_IME_</ime>
        <preziem>_PREZIME_</preziem>
        <tel>_BROJ_0_</tel>
        <tel>_BROJ_N_</tel>
        <email>_IMEJL_0_</email>
        <email>_IMEJL_N_</email>
    </kontakt>
</imenik>
```
Značenje polja:
* _IME_ - Ime kontakta (obavezno polje)
* _PREZIME_ - Prezime kontakta (obavezno polje)
* _BROJ_x_ - Nula ili više brojeva kontakta
* _IMEJL_x_ - Nula ili više imejl adresa kontakta

Primer .xml fajla dat je u `src/main/resources/imenik.xml`.

## Uraditi
Potrebno je napisati klasu `ImenikImpl` koja implementira interfejs `IImenik`.  
implementirati sledeće metode:
* Metoda `void ucitajXmlImenik(String filePath) throws IOException`  
Učitava imenik iz .xml fajla. Kao parametar prima putanju do fajla.  
U slučaju da ne može da pročita fajl baca izuzetak `IOException`.  
Imenik se učitava u privatnu promenljivu tipa lista (lista kontakata).  
* Metoda `void sacuvajXmlImenik(String filePath) throws IOException`  
Pozivom ove metode čuva se sadržaj imenika u .xml.  
Kao parametar se prosleđuje putanja do fajla.  
* Metoda `void dodajKontakt(Kontakt kontakt)`  
Dodaje kontakt u imenik.  
* Metoda `List<Kontakt> nadjiKontakt(String ime, String prezime)`  
Pozvivom ove metode vrši se pretraga imenika.  
Kao parametri se prosleđuje ime i prezime kontakta.  
Kao rezultat vratiti samo one kontakte koji kod kojih postoji potpuno poklapanje sa traženim vrednostima.  
U slučaju da nije pronažen ni jedan kontakt vratiti **praznu listu**.  
* Metoda `List<Kontakt> nadjiKontaktTel(String tel)`  
Ova metoda vraća listu kontakata koji sadrže traženi broj telefona.  
U slučaju da nije pronažen ni jedan kontakt vratiti **praznu listu**.  
* Metoda `List<Kontakt> nadjiKontaktImejl(String imejl)`  
Ova metoda vraća listu kontakata koji sadrže traženi imejl.  
U slučaju da nije pronažen ni jedan kontakt vratiti **praznu listu**.  
* Metoda `void obrisiKontakt(String ime, String prezime)`  
Ova metoda iz imenika briše sve kontakte sa zadatim imenom.  

Za čitanja i čuvanje imenika u .xml formatu može se koristiti DOM, SAX i StAX parser (po slobodnom izboru).

## Razmisliti
U ovom primeru dat je rad sa .xml fajlovima.  
Šta bi bilo potrebno izmeniti u interfejsu tako da može da radi i .json formatom.
